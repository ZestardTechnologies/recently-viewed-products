@extends('header')
@section('content')
<?php
	$attribute_count=0;
	$temp_ids=array();
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>  
<script type="text/javascript">
var length, product_data;
product_ids_array = new Array();
colors = new Array("red","lime","gold","violet","#52c1bc","#007ace");   	
ShopifyApp.ready(function(e){
	ShopifyApp.Bar.initialize({
		buttons: {		
			secondary: [
			{
				label: 'General Settings',
				href : 'dashboard',
				loading: true
			},
			{
				label: 'Track Products',
				href : 'track-products',
				loading: true
			},
			{
				label: 'HELP',
				href : '{{ url('/help') }}',
				loading: true
			}]
		}    
	});
	
	var shopifyQL = "SHOW product_id FROM sales UNTIL today ORDER BY product_id ASC";
	var renderData = function(response){														
		length = response.result['data'].length;		
		for(i = 0; i<length; i++)
		{				
			if(response.result['data'][i]['0'] == 0)
			{
				
			}
			else
			{
				product_ids_array.push(response.result['data'][i]['0']);
			}
		}
		$.ajax({
			url: "get-product-data",
			async: false,
			data :{ products: product_ids_array, _token: "{{ csrf_token() }}" },
			success: function(result)	
			{
				product_data = result;
			}
		});				
	// Load the Visualization API and the line package.	
	google.charts.load('current', {'packages':['corechart', 'bar']});
    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);
		
    function drawChart() 
	{    
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable();
  
		data.addColumn('string', 'Product');
		data.addColumn('number', 'Quantity sold');      
        
		var jsonData = $.parseJSON(product_data);

		for (var i = 0; i < jsonData.length; i++) {
			data.addRow([jsonData[i].product_name, parseInt(jsonData[i].quantity_sold)]);
		}
		var options = {			
			title: 'Best Selling Products',
			subtitle: '',
			fontSize: 16,			
			height: 500,
			axes: {			
			 
			},			
			hAxis: {title: 'Quantity Sold', titleTextStyle: {color: 'black', fontSize:'18', bold:true}},
			vAxis: {title: 'Products', titleTextStyle: {color: 'black', fontSize:'18', bold:true}},
			colors: ["#89b8d9","red","lime","gold","violet","#52c1bc"]								 
		};
		//var chart = new google.charts.BarChart(document.getElementById('bar_chart'));
		var chart1 = new google.visualization.LineChart(document.getElementById("line_chart"));
		var chart = new google.visualization.BarChart(document.getElementById("bar_chart"));
		chart.draw(data, options);  
		chart1.draw(data, options);  
	}	
	};
	var handleError = function(response) {
		//Sending mail regarding error generated
		$.post("acknowledge",
				{ response: response, _token: "{{ csrf_token() }}" },
			   function(success){					   					   
			   });
		
	};
	ShopifyApp.Analytics.fetch({
	  query: shopifyQL,
	  success: renderData,
	  error: handleError
	});		
});
</script>
<div class="dashboard container">	
	<div class="col-md-12 left-content-setting">
		<div class="subdiv-content settings">		
			<h2 class="sub-heading message">Track Products</h2>        									
			<div class="graphs">
				<div id="line_chart">			
				</div>				
				<div id="bar_chart">			
				</div>				                         		                           
			</div>				                         		                           
		</div>				                         		                           
	</div>    
</div>
@endsection

