@extends('header')
@section('content')
<?php
	$store_name = session('shop');	   					

	$app_status = '';
	$slider_title = '';
	$slider_subtitle = '';
	$number_of_products = '';
	$autoplay_slider = '';
	$display_border = '';
	$border_style = '';
	$border_color = '';
	$border_size = '';		
	$product_redirect =1;
	$select_products = '';
	$animation_loop = '';	
	$persist_day_week_month = 1;
	$day_week_month = 1;
	if(count($settings) > 0)
	{			
		$app_status = $settings[0]->app_status;
		$slider_title = $settings[0]->slider_title;
		$slider_subtitle = $settings[0]->slider_subtitle;
		$number_of_products = $settings[0]->number_of_products;
		$autoplay_slider = $settings[0]->autoplay_slider;
		$display_border = $settings[0]->display_border;
		$border_style = $settings[0]->border_style;
		$border_color = $settings[0]->border_color;
		$border_size = $settings[0]->border_size;
		$select_products = $settings[0]->select_products;
		$animation_loop = $settings[0]->loop;
		$product_redirect = $settings[0]->product_click;		
		$persist_day_week_month = $settings[0]->persist_day_week_month;		
		$day_week_month = $settings[0]->day_week_month;		
	}	
?>
<script type="text/javascript">
        var length;
        product_ids_array = new Array();
	ShopifyApp.ready(function(e){
		ShopifyApp.Bar.initialize({
		  buttons: {
			primary: {
			  label: 'SAVE',
			  message: 'form_submit',
			  callback:	function(event){ 						
						var product_no = parseInt($(".product_no").val());
						var slider_title = $(".slider_title").val();							
												
						if(slider_title == '' || typeof slider_title === 'undefined')
						{
							alert('Please Enter Slider Title');							
						}
						else
						{
							if(product_no == '' || typeof product_no === 'undefined')
							{
								alert('Please Enter Number of Products to Display');
								$(".product_no").focus();			
							}
							else
							{
								var no = parseInt(product_no);
								if(no >= 1 && no <= 6)
								{
									$("#slider").attr("data-shopify-app-submit", "form_submit");	
								}
								else
								{
									alert('Please Enter Any Number Between 1 To 6');
									$(".product_no").val("");								
								}
							}	
						} } 
			},
			secondary: [{
			label: 'General Settings',
			href : 'dashboard',
			loading: true
			},
			{
				label: 'HELP',
				href : '{{ url('/help') }}',
				loading: true
			}]
		}    
	});				
	});	
	//For performing Validations
	function validate(event)
	{		
		var product_no = $(".product_no").val();
		var slider_title = $(".slider_title").val();
		var slider_subtitle = $(".slider_subtitle").val();
		if(slider_title == '' || typeof slider_title === 'undefined')
		{
			alert('Please Enter Slider Title');
			event.preventDefault();
		}		
		if(product_no == '' || typeof product_no === 'undefined')
		{
			alert('Please Enter Number of Products to Display');
			$(".product_no").focus();
			event.preventDefault();
		}
		else
		{
			var no = parseInt(product_no);
			if(no >= 1 && no <= 6)
			{				
			}
			else
			{
				alert('Please Enter Any Number Between 1 To 6');
				$(".product_no").val("");
				event.preventDefault();
			}
		}							
	}
	
	$(document).ready(function(){
		$(".border_color").spectrum({
			preferredFormat: "hex",
			showInput: true,
			showAlpha: true,
			showPalette: true,
			palette: [],        
		});	
		$(".recently_viewed_slider").show();
		
		/* Loading Slider with Images */		
			for(var i=0;i<6; i++)
			{				
				$(".recently_viewed_slider").append("<div><img style='max-width: 200px; !important; margin:0 auto;max-height:200px !important;' src='{{ asset('image/400.jpg')  }}'/></div>");	
			}		
		
		/* Setting Height and Width of Slider Images */		
		//$(".slides li img").css("height","160px");
		
		var color, size, style;
		
		/* Creating Slider */
		
		@if(count($settings) <= 0)			
			$('.recently_viewed_slider').slick({
				dots: true,
				infinite: false,
				speed: 300,
				slidesToShow: 	3,
				slidesToScroll: 3,
				responsive: [
					{
						breakpoint: 1024,
						settings: {
							slidesToShow: 	3,
							slidesToScroll: 3,
							infinite: true,
							dots: true
						}
					},
					{
						breakpoint: 600,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}			
				]
			}); 
						
		@else			
			var autoplay,loop;
			@if($settings[0]->autoplay_slider == 1)
				autoplay = true;
			@else
				autoplay = false;
			@endif

			@if($settings[0]->loop == 1)
				loop = true;
			@else
				loop = false;
			@endif
			/* 
				$('.recently_viewed_slider').flexslider({
					animation: "slide",
					animationLoop: loop,				
					slideshow: autoplay,
					itemWidth: 410,
					itemMargin: 25,				
					minItems: {{ $settings[0]->number_of_products }},
					maxItems: {{ $settings[0]->number_of_products }}
				}); 
			*/
			
			$('.recently_viewed_slider').slick({
				dots: true,
				infinite: false,
				speed: 300,	
				slidesToShow: 	{{ $settings[0]->number_of_products }},
				slidesToScroll: {{ $settings[0]->number_of_products }},	
				responsive: [
					{
						breakpoint: 1024,
						settings: {
							slidesToShow:	{{ $settings[0]->number_of_products }},
							slidesToScroll: {{ $settings[0]->number_of_products }},
							infinite: true,
							dots: true
						}
					},
					{
						breakpoint: 600,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}			
				]
			}); 
			
			@if($settings[0]->display_border == 1)
				color = "{{ $settings[0]->border_color }}"
				style = "{{ $settings[0]->border_style }}"
				size  = "{{ $settings[0]->border_size }}"
				$(".recently_viewed_slider").css("border", size + " " + style + " " + color);	
			@else
				$(".recently_viewed_slider").css('border','none');				
			@endif
									
		@endif
		/* $(".flex-next").html("<img height=20 width=20 src='https://zestardshop.com/shopifyapp/quick_buy/public/image/right.png' />");
		$(".flex-prev").html("<img height=20 width=20 src='https://zestardshop.com/shopifyapp/quick_buy/public/image/left.png' />");		
		$(".flex-next").css("right","0");
		$(".flex-next").css("top","30%");
		$(".flex-next").css("position","absolute");
		$(".flex-next").css("font-size","2em");
		$(".flex-prev").css("left","0");
		$(".flex-prev").css("position","absolute");
		$(".flex-prev").css("font-size","2em");
		$(".flex-prev").css("top","30%"); */
		
		$(".recently_viewed_slider").css("border", size + " " + style + " " + color);				 
			var title = $(".slider_title").val();	
			$(".preview_title").html(title);					
			var sub_title = $(".slider_subtitle").val();	
			$(".preview_subtitle").html(sub_title);			
			
			$(".slider_title").keyup(function(){
				var title = $(this).val();	
				$(".preview_title").html(title);			
			});
			$(".slider_subtitle").keyup(function(){
				var sub_title = $(this).val();	
				$(".preview_subtitle").html(sub_title);			
			});
			$(".border_color").change(function(){	
				color=$(this).val();
				size=$('.border_size').val();
				style=$('.border_style').val();
				$(".recently_viewed_slider").css("border", size + " " + style + " " + color);
			});	
			$(".border_size").change(function(){				
				color=$('.border_color').val();
				size=$(this).val();
				style=$('.border_style').val();
				$(".recently_viewed_slider").css("border", size + " " + style + " " + color);
			});	
			$(".border_style").change(function(){				
				color=$('.border_color').val();
				style=$(this).val();
				size=$('.border_size').val();
				$(".recently_viewed_slider").css("border", size + " " + style + " " + color);
			});			
			$("#checkboxID4").change(function(){			
				if($(this).prop("checked"))
				{
					$(".recently_viewed_slider").css("border", size + " " + style + " " + color);
				}
				else
				{				
					$(".recently_viewed_slider").css('border','none');
				}		
			});
		}); 					
</script>
<div class="row">
<div class="dashboard container">	
	<div class="col-md-12 left-content-setting">
		<div class="subdiv-content settings">
		<div class="modal slide" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">              
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">×</span>
							<span class="sr-only">Close</span>
						</button>
						<img src="" class="imagepreview" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
		<h2 class="sub-heading">General Settings</h2>        
			<form action="save-recently-viewed" method="post" name="slider" id="slider" data-shopify-app-submit="">                
			{{ csrf_field() }}
			<div class="col-sm-4 form-group">
				<label for="status">Enable App?</label>
				<span class="onoff">
                                        <input name="status" type="checkbox" value="1" @if($app_status == 1) checked @endif id="checkboxID0">
					<label for="checkboxID0"></label>
				</span>					
			</div>   			
			<div class="col-sm-12 form-group">				
				<label for="slider_title">Slider Title</label>
				<input name="slider_title" type="text" value="{{ $slider_title }}" class="validate form-control slider_title" required />			
			</div>
			<div class="col-sm-12 form-group">					
				<label for="slider_subtitle">Slider Subtitle</label>
				<textarea name="slider_subtitle" style="resize: none;" class="form-control slider_subtitle" rows="2">{{ $slider_subtitle }}</textarea>
			</div>
			<div class="col-sm-12 form-group">					
				<label for="slider_subtitle">On Clicking Product&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<label class="cursor_pointer"><input type="radio" @if($product_redirect == 1) checked @endif name="product_click" value="1">&nbsp;Redirect to Product Page&nbsp;&nbsp;&nbsp;&nbsp;</input></label>
				<label class="cursor_pointer"><input type="radio" @if($product_redirect == 0) checked @endif name="product_click" value="0">&nbsp;Show Pop-Up</input></label>
			</div>							
			<div class="col-sm-3 form-group">
				<label for="product">No. of Products to Display</label>
				<input name="product_no" value="{{ $number_of_products }}" type="number" min="1" max="6" class="form-control product_no" required />
			</div>
			<div class="border_div">										
				<div class="col-sm-3 form-group">						
					<label for="status">Border Style</label>					
					<select class="form-control border_style" name="border_style">
						<option @if($border_style == "solid") selected @endif value="solid">Solid</option>
						<option @if($border_style == "dashed") selected @endif value="dashed">Dashed</option>
						<option @if($border_style == "dotted") selected @endif value="dotted">Dotted</option>
						<option @if($border_style == "groove") selected @endif value="groove">Groove</option>
						<option @if($border_style == "ridge") selected @endif value="ridge">Ridge</option>
						<option @if($border_style == "inset") selected @endif value="inset">Inset</option>
						<option @if($border_style == "outset") selected @endif value="outset">Outset</option>
					</select>						
				</div>						
				<div class="col-sm-3 form-group">							
					<label for="status">Border Size</label>								
					<select class="form-control border_size" name="border_size">
						<option @if($border_size == "1px") selected @endif value="1px">1px</option>							
						<option @if($border_size == "2px") selected @endif value="2px">2px</option>							
						<option @if($border_size == "3px") selected @endif value="3px">3px</option>							
						<option @if($border_size == "4px") selected @endif value="4px">4px</option>							
					</select>					
				</div>
				<div class="col-sm-3 form-group">
					<label for="status">Border Color</label>
					<br>
					<input class="form-control border_color" @if(!empty($border_color)) value="{{ $border_color }}" @else value="{{ $border_color }}" @endif type="text" name="border_color"/>	
				</div>	
			<div class="col-sm-4 form-group text-left">							
			<label for="status">Autoplay Slider<br></label>
			<span class="onoff">
				<input name="autoplay" type="checkbox" value="1" @if($autoplay_slider == 1) checked @endif id="checkboxID2">
				<label for="checkboxID2"></label>
			</span>					
			</div>	
			<div class="col-sm-4 form-group text-left">				
			<label for="status">Loop<br></label>
			<span class="onoff">
				<input name="loop" type="checkbox" value="1" @if($animation_loop == 1) checked @endif id="checkboxID3"/>
				<label for="checkboxID3"></label>
			</span>					
			</div>
                            
                        <div class="col-sm-4 form-group text-left">							
			<label for="status">Display Border?</label>
			<span class="onoff">
				<input name="border_status" type="checkbox" value="1" @if($display_border == 1) checked @endif id="checkboxID4">
				<label for="checkboxID4"></label>
			</span>					
			</div>
			<div class="col-sm-6 form-group">					
				<label for="slider_subtitle">How Long to Persist Recently Viewed Products?&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<input name="persist_day_week_month" value="{{ $persist_day_week_month }}" type="number" min="1" class="form-control persist_day_week_month" required />	
			</div>
			<br>			
			<br>			
			<br>			
			<div class="col-sm-6 form-group">
				<br>
				<label class="cursor_pointer"><input type="radio" @if($day_week_month == 1) checked @endif name="day_week_month" value="1">&nbsp;Days&nbsp;&nbsp;&nbsp;&nbsp;</input></label>
				<label class="cursor_pointer"><input type="radio" @if($day_week_month == 2) checked @endif name="day_week_month" value="2">&nbsp;Weeks&nbsp;&nbsp;&nbsp;&nbsp;</input></label>
				<label class="cursor_pointer"><input type="radio" @if($day_week_month == 3) checked @endif name="day_week_month" value="3">&nbsp;Months</input></label>
			</div>			
			</div>					
			</form>	
			<br>		                        
			<div class="col-sm-12 form-group text-center preview_box">					
				<u><h1>Preview</h1></u>
				<h3 class="preview_title">
				</h3>
				<h4 class="preview_subtitle">
				</h4>											
				<div class="recently_viewed_slider flexslider carousel"> <!-- Slider class for slick slider -->
				</div>					
			</div>
		</div>				                         		                           		
	</div>    
	<div class="col-md-12 formcolor sticky_formcolor" >							
		<?php
			if(Session::has('shop')){
				$url = "https://".session('shop')."/admin/themes/current/?key=templates/cart.liquid";
			}
			else{
				$url = "#";
			}
		?>
		<?php
			if(Session::has('shop')){
				$url1 = "https://".session('shop')."/admin/themes/current/?key=sections/cart-template.liquid";
			}
			else{
				$url1 = "#";
			}
		?>						
		<div class="shortcode_heading col-sm-6">
		<h2 class="sub-heading subleft col-md-3">Shortcode</h2>
			<div class="col-sm-9">
				<div class="copystyle_wrapper">
					<textarea rows="1" class="form-control script_code" id="script_code" disabled><?php echo "{% include 'recently-viewed' %}" ?></textarea>
					<btn id="copy_script" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_script()"><i class="fa fa-check"></i> Copy</btn>
				</div>
			</div>
		</div>
		<div class="shortcode_heading col-sm-6">
		<h2 class="slide_down sub-heading subleft col-md-12">Where to paste Shortcode?<i class="fa fa-chevron-up"></i></h2>
			<div class="col-sm-12" id="shortcode_info">          
				<ul class="shortcode-note">
					<li>After copying shortcode, paste the shortcode in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=layout/theme.liquid" target="_blank"><b>theme.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" data-src="{{ asset('image/recent_view_screen.png') }}" data-toggle="modal" data-target="#help_modal"><b> See Example</b></a></li>					
				</ul>           
			</div>
		</div>          
    </div>     	        
</div>
</div>
<div class="modal fade" id="help_modal" role="dialog">
    <div class="modal-dialog">      
		<div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Help</h4>
			</div>
			<img src=""/>			
		</div>      
    </div>
</div>
<div class="modal fade" id="new_note">
    <div class="modal-dialog">          
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><b>Note</b></h4>
			</div>
			<div class="modal-body">
				<p>Dear Customer, As this is a paid app and hundreds of customers are using it, So if you face any issue(s) on your store, before uninstalling, Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right to resolve it ASAP.</p>
			</div>        
			<div class="modal-footer">			
				<div class="datepicker_validate" id="modal_div">
					<div>
						<strong style="margin-right:30px">Show me this again</strong>
							<span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again"/>							
							<label for="dont_show_again"></label></span>
					</div>      
				</div>      
			</div>      
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$(".screenshot").click(function(){
		$(".modal-content img").attr("src", $(this).attr("data-src"));
	});
	$(".slide_down").click(function(){		
		var display = $("#shortcode_info").css("display");	    
		if(display == "none")
		{			
			$(".slide_down i").removeClass("fa fa-chevron-up");
			$(".slide_down i").addClass("fa fa-chevron-down");
		}
		else
		{
			$(".slide_down i").removeClass("fa fa-chevron-down");
			$(".slide_down i").addClass("fa fa-chevron-up");
		}
		$("#shortcode_info").slideToggle();		
	});
	$("#dont_show_again").change(function(){
		var checked = $(this).prop("checked");
		var shop_name = "{{ session('shop') }}";
		if (!checked)
		{
			$.ajax({
				url: 'update-modal-status',
				data: {shop_name: shop_name},
				async: false,				
				success: function (result)
				{

				}
			});
		$('#new_note').modal('toggle');
		}
	});
});
var new_install = "{{ $new_install }}";	
if(new_install == "Y")
{
	$('#new_note').modal('show');
}
</script>
@endsection