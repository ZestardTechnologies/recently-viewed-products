@extends('header')
@section('content')

<script type="text/javascript">
  ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({   
      buttons: {        
        secondary: [
		{
			label: 'General Settings',
			href : 'dashboard',
			loading: true
		},		
		{
			label: 'HELP',
			href : '{{ url('/help') }}',
			loading: true
		}]
        }    
    });
});
$(document).ready(function(){
	$(".screenshot").click(function(){
		$(".modal-content img").attr("src", $(this).attr("data-src"));
	});
});
</script>

<?php
	$store_name = session('shop');	   					
?>

<div class="container formcolor formcolor_help" >
    <div class=" row">
        <div class="help_page">
			<div class="col-md-12 col-sm-12 col-xs-12 need_help">
				<h2 class="dd-help">Need Help?</h2>
				<p class="col-md-12"><b>To customize any thing within the app or for other work just contact us on below details</b></p>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<ul class="dd-help-ul">
						<li><span>Developer: </span><a target="_blank" href="https://www.zestard.com">Zestard Technologies Pvt Ltd</a></li>
						<li><span>Email: </span><a href="mailto:support@zestard.com">support@zestard.com</a></li>
						<li><span>Website: </span><a target="_blank" href="https://www.zestard.com">https://www.zestard.com</a></li>
					</ul>
				</div>
			</div>
			 
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h2 class="dd-help">General Instruction</h2> 
				<div class="col-md-12 col-sm-12 col-xs-12 help_accordians">					 
					 <div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
										<strong><span class="">Where to paste shortcode?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  
									</p>
								</h4>
							</div>
							<div id="collapse1" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="ul-help">
										<li>After copying shortcode, paste the shortcode in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=layout/theme.liquid" target="_blank"><b>theme.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" data-src="{{ asset('image/recent_view_screen.png') }}" data-toggle="modal" data-target="#help_modal"><b> See Example</b></a></li>					
										<!--li>After copying shortcode, you can paste it in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=templates/cart.liquid" target="_blank"><b>cart.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" data-src="{{ asset('image/cart.png') }}" data-toggle="modal" data-target="#help_modal"><b> See Example</b></a></li>
										<li>If your theme is section theme then paste it in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=sections/cart-template.liquid" target="_blank"><b>cart-template.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" data-src="{{ asset('image/cart_template.png') }}" data-toggle="modal" data-target="#help_modal"><b> See Example</b></a></li-->		   					
									</ul>									
								</div>
							</div>
						</div>
					 </div> 
					<div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<p data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 
										<strong><span class="">What is Slider Title and Subtitle</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  
									</p>
								</h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="ul-help">
										<li>Slider Title is title for your product's slider which will be displayed above slider</li>
										<li>Slider Subtitle is subtitle or tagline for your product's slider which will be displayed above slider and below slider title</li>										   
									</ul>									
								</div>
							</div>
						</div>
					 </div>
					 <div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
										<strong><span class="">What happens when user clicks on product?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  
									</p>
								</h4>
							</div>
							<div id="collapse3" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="ul-help">
										<li>There are two options:</li>
										<li>Either you can show new page with product details</li>
										<li>Or you can show him product details on same page. In this option details will be displayed in a subpage</li>    
									</ul>									
								</div>
							</div>
						</div>
					 </div>
					 <div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<p data-toggle="collapse" data-parent="#accordion" href="#collapse4"> 
										<strong><span class="">What is the meaning of "No. of products to display" ?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  
									</p>
								</h4>
							</div>
							<div id="collapse4" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="ul-help">
										<li>It specifies how many products will be displayed in slider(Maximum 6)</li>										
									</ul>									
								</div>
							</div>
						</div>
					 </div>
					 <div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<p data-toggle="collapse" data-parent="#accordion" href="#collapse5"> 
										<strong><span class="">What is "Autoplay slider" & "Loop"?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  
									</p>
								</h4>
							</div>
							<div id="collapse5" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="ul-help">
										<li>Autoplay slider means whether to rotate through products automatically or manually. If yes, Then products will be rotated automatically</li>										
										<li>Loop means whether to repeat rotating through products. If yes, Then products will be rotated automatically again and again</li>										
									</ul>									
								</div>
							</div>
						</div>
					 </div>
					 <div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<p data-toggle="collapse" data-parent="#accordion" href="#collapse6"> 
										<strong><span class="">What is "How Long to Persist Recently Viewed Products"?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  
									</p>
								</h4>
							</div>
							<div id="collapse6" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="ul-help">
										<li>It means for how many days/weeks/months you want user to see his/her recently viewed products</li>										
									</ul>									
								</div>
							</div>
						</div>
					 </div>
					 <div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<p data-toggle="collapse" data-parent="#accordion" href="#collapse8"> 
										<strong><span class="">How to uninstall app?</span>
										<span class="fa fa-chevron-down pull-right"></span></strong>  
									</p>
								</h4>
							</div>
							<div id="collapse8" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="ul-help">
										<li>To remove the App, go to <a href="https://<?php echo $store_name;?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
										<li>Click on delete icon of Recently Viewed Products on Cart Page App.<a class="screenshot" href="javascript:void(0)" data-src="{{ asset('image/recent_view_delete.png') }}" data-toggle="modal" data-target="#help_modal"><b>See Example</b></a></li>
										<li>If possible then remove shortcode where you have pasted. As you have pasted the shortcode in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=layout/theme.liquid" target="_blank"><b>theme.liquid</b></a> remove from there.<a class="screenshot" href="javascript:void(0)" data-src="{{ asset('image/recent_view_screen_remove.png') }}" data-toggle="modal" data-target="#help_modal"><b> See Example</b></a></li>
									</ul>									
								</div>
							</div>
						</div>
					 </div>
				</div>
			</div>
		</div>      
    </div>
</div>
<div class="modal fade" id="help_modal" role="dialog">
    <div class="modal-dialog">      
		<div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Help</h4>
			</div>
			<img src=""/>			
		</div>      
    </div>
</div>
<script>
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".fa").addClass("fa-chevron-up").removeClass("fa-chevron-down");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find("span.fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find("span.fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        });
    });
</script>

@endsection

