@yield('header')
<!DOCTYPE html>
<html lang="en">
	<head>	
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Recently Viewed Products</title>	
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- toastr CSS -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"> 

    <!-- magnificent popup CSS -->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    
    <link rel="stylesheet" href="{{ asset('css/simple-slider.css') }}">
	
    <!-- custom CSS -->
    <!-- <link rel="stylesheet" href="{{ asset('css/style.css') }}"> -->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    
	<!-- For Slick Slider  -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/slick-theme.css') }}">
	<!-- For Slick Slider  -->		
	
	<!-- Spectrum color picker -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/spectrum.css') }}">
	<!-- Spectrum color picker -->
	
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
        
    <!-- shopify Script for fast load -->
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    
	<!-- For Slick Slider  -->
	<script src="{{ asset('js/slick.js') }}" type="text/javascript" charset="utf-8"></script>
	<!-- For Slick Slider  -->
	
	<!-- For Flex Slider  -->	
	<script src="{{ asset('js/slick.js') }}" type="text/javascript" charset="utf-8"></script>	
	<!-- For Flex Slider  -->
	
	<!-- For Spectrum color picker -->
	<script src="{{ asset('js/spectrum.js') }}" type="text/javascript" charset="utf-8"></script>
	<!-- For Spectrum color picker  -->
	
	<!-- For Datatable -->
    <script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatable/dataTables.buttons.min.js') }}"></script>
    <!-- For Datatable -->
	
<script>
	ShopifyApp.init({
		apiKey: '55ee75badaf59b6712906ae6437a05dd',
		shopOrigin: '<?php echo "https://".session('shop') ?>'
	});

	ShopifyApp.ready(function() {
		ShopifyApp.Bar.initialize({
			icon: "",
			title: '',
			buttons: {}
		});
	});		
</script>

</head>

<body>
@yield('navigation')
<div class="overlay"></div>
@yield('content')

<script src="{{ asset('js/jquery.copy-to-clipboard.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/simple-slider.js') }}"></script>
<script src="{{ asset('js/javascript.js') }}"></script>

<script type="text/javascript">
  function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    //alert('Copied!');
    $temp.remove();
  }      
</script>

<script>
  jQuery(document).ready(function(){       
    jQuery(".copyMe").click(function (){
      var count = jQuery('.show').length;
      if(count == 0){
        jQuery(".show").show();
        jQuery(".success-copied").after('<div class="alert alert-success alert-dismissable show"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Success!</strong> Your shortcode has been copied.</div>');
      }
    });
  });
</script>

<script>
	@if(Session::has('notification'))

    var type = "{{ Session::get('notification.alert-type', 'info') }}";
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('notification.message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('notification.message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('notification.message') }}");
            break;         
        case 'options':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;

    }
@endif
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a2e20e35d3202175d9b7782/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

</body>
</html>
