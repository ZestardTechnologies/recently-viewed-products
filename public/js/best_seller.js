var shop_name = Shopify.shop;
var base_path_best_seller = "https://zestardshop.com/shopifyapp/best_seller/public/";
var products_array = undefined, settings, parsed_data;
var jscript = document.createElement("script");
var flexslider_script = document.createElement("script");
var modal_script = document.createElement("script");
jscript.src = base_path_best_seller + "js/zestard_jquery_3.3.1.js";	
jscript.type = 'text/javascript';
jscript.async = false;		

var el = document.getElementById('best_seller_slider'),
elChild = document.createElement('div');
elChild.id = 'jquery_box';		
el.insertBefore(elChild, el.firstChild); 	
document.getElementById('jquery_box').append(jscript);	
document.getElementById('jquery_box').append(jscript);	

jscript.onload = function() {
	$zestard_best_seller = window.jQuery;				
	flexslider_script.src = base_path_best_seller + "js/jquery.flexslider.js";		
	flexslider_script.type = 'text/javascript';
	flexslider_script.async = false;			
	document.getElementById('best_seller_slider').append(flexslider_script);
	flexslider_script.onload = function() {					
		modal_script.src = base_path_best_seller + "js/jquery.modal.min.js";		
		modal_script.type = 'text/javascript';
		modal_script.async = false;									
		document.getElementById('best_seller_slider').append(modal_script);	
		modal_script.onload = function() {
			$zestard_best_seller.ajax({
				url: base_path_best_seller + "best-seller",
				data: {
					shop_name: shop_name
				},
				crossDomain: true,
				async: false,
				success: function(result) {			
					if(result)
					{
						$zestard_best_seller("#best_seller_slider").append(result);
						//document.getElementById('best_seller_slider').append(result);						
					}			
				}
			});
		};
	};
};