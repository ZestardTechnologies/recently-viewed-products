var date = new Date();
var shop_name = Shopify.shop;
var $zestard_recent_product = "";
var base_path_recent_view = "https://zestardshop.com/shopifyapp/recently-viewed-products/public/";
var products_array = undefined, settings, persist_day_week_month, day_week_month, expiry, call_flag = 0, parsed_data, timestamp, current_timestamp, shop_currency;
var el = document.getElementById('recent_view_slider'),
        elChild = document.createElement('div');
elChild.id = 'recent_view_jquery_box';
el.insertBefore(elChild, el.firstChild);
var modal_script = document.createElement("script");
$zestard_recent_product = window.jQuery;
$zestard_recent_product.ajax({
    url: base_path_recent_view + "get-settings",
    data: {
        shop_name: shop_name
    },
    crossDomain: true,
    async: false,
    success: function (result) {
        settings = JSON.parse(result);
    }
});
if (settings.length > 0) {
    persist_day_week_month = settings[0].persist_day_week_month;
    day_week_month = settings[0].day_week_month;
    //For Days
    if (day_week_month == 1) {
        expiry = Date.now() + persist_day_week_month * 24 * 60 * 60;
    }
    //For Weeks
    if (day_week_month == 2) {
        expiry = Date.now() + persist_day_week_month * 7 * 24 * 60 * 60;
    }
    //For Months
    if (day_week_month == 3) {
        expiry = Date.now() + persist_day_week_month * 30 * 24 * 60 * 60;
    }
} else {
    expiry = Date.now() + 1 * 24 * 60 * 60;
}
if (settings.length > 0) {
    if (settings[0].app_status == 1) {
        modal_script.src = base_path_recent_view + "js/jquery.modal.min.js";
        modal_script.type = 'text/javascript';
        modal_script.async = false;
        document.getElementById('recent_view_jquery_box').append(modal_script);
        modal_script.onload = function () {
            $zestard_recent_product = window.jQuery;
            save_recent_products();
        };
    } else {
        $zestard_recent_product("#recent_div_box").remove();
    }
} else {
    $zestard_recent_product("#recent_div_box").remove();
}

function save_recent_products() {
    if (product_page == 1) {
        if (product_id != "" || product_id != null || typeof product_id !== 'undefined') {
            if (localStorage.getItem('sessionStorage') == null || localStorage.getItem('sessionStorage') == "" || typeof localStorage.getItem('sessionStorage') === 'undefined')
                    //if(Cookies.get('sessionStorage') == null || Cookies.get('sessionStorage') == "" || typeof Cookies.get('sessionStorage') === 'undefined')
                    {
                        call_flag = 0;
                        localStorage.setItem('sessionStorage', product_id);
                        //Cookies.set('sessionStorage', product_id, {expiry : expiry});					  

                        product_ids = localStorage.getItem('sessionStorage');
                        //product_ids = Cookies.get('sessionStorage');									  
                        $zestard_recent_product.ajax({
                            url: base_path_recent_view + 'recent-product',
                            data: {product_ids: product_ids, shop_name: shop_name, expiry: expiry},
                            async: false,
                            type: 'POST',
                            success: function (result) {
                                products_array = result;
                            }
                        });
                        localStorage.setItem('product_data', products_array);
                    } else {
                call_flag = 1;
                product_ids = localStorage.getItem('sessionStorage');
                //product_ids = Cookies.get('sessionStorage');					
                if (product_ids.indexOf(product_id) == -1) {
                    $zestard_recent_product.ajax({
                        url: base_path_recent_view + 'recent-product',
                        data: {product_ids: product_ids, shop_name: shop_name, expiry: expiry},
                        async: false,
                        type: 'POST',
                        success: function (result) {
                            products_array = result;
                        }
                    });
                    localStorage.setItem('product_data', products_array);
                    product_ids = product_ids + "," + product_id;
                    product_ids_array = product_ids.split(",");
                    if(product_ids_array.length > 6) 
					{
                        if(shop_name == "true-african-art-com.myshopify.com")
						{
							
						}
						else
						{
							product_ids_array.splice(0, 1);
						}
                    }
                    localStorage.setItem('sessionStorage', product_ids_array.join());
                    //Cookies.set('sessionStorage', product_ids_array.join(), {expiry : expiry});
                    //product_ids = localStorage.getItem('sessionStorage');								
                    //product_ids = Cookies.get('sessionStorage');													
                    //localStorage.setItem('sessionStorage', product_ids);										
                } else {
                    //call_flag = 1;
                    //product_ids = Cookies.get('sessionStorage');								
                    product_ids = localStorage.getItem('sessionStorage');

                    $zestard_recent_product.ajax({
                        url: base_path_recent_view + 'recent-product',
                        data: {product_ids: product_ids, shop_name: shop_name, expiry: expiry},
                        async: false,
                        type: 'POST',
                        success: function (result) {
                            products_array = result;
                        }
                    });
                    localStorage.setItem('product_data', products_array);
                }
            }
        }
    } else {
        //if(Cookies.get('product_data') == null || Cookies.get('product_data') == "" || typeof Cookies.get('product_data') === 'undefined')
        if (localStorage.getItem('product_data') == null || localStorage.getItem('product_data') == "" || typeof localStorage.getItem('product_data') === 'undefined') {
        } else {
            //products_array = Cookies.get('product_data');
            call_flag
            products_array = localStorage.getItem('product_data');
            //show_recent_products(products_array);	
        }
    }
    /* 
     if(typeof products_array !== 'undefined' || !products_array)
     {
     localStorage.setItem('product_data', products_array);										 
     } 
     */
    if (call_flag == 1) {
        //if(cart_page == 1)
        {
            show_recent_products(products_array);
        }
    }
}

function show_recent_products(products_array) {
    //recently_viewed_products = JSON.parse(products_array); 		
    recently_viewed_products = $zestard_recent_product.parseJSON(products_array);
    recently_viewed_products = recently_viewed_products.product_data;
    if (settings.length > 0) {
        if (settings[0].app_status == 1) {
            if (recently_viewed_products) {
                color = settings[0].border_color;
                display_border = settings[0].display_border;
                style = settings[0].border_style;
                size = settings[0].border_size;
                products = settings[0].number_of_products;
                autoplay = settings[0].autoplay_slider;
                slider_title = settings[0].slider_title;
                slider_subtitle = settings[0].slider_subtitle;
                shop_currency = settings[0].shop_currency;
                loop = settings[0].loop;
                product_click = settings[0].product_click;
                length = recently_viewed_products.length;
                $zestard_recent_product(window).on('resize', function () {});
                if (length > 0) {
                    if (autoplay == 1) {
                        autoplay = true;
                    } else {
                        autoplay = false;
                    }
                    if (loop == 1) {
                        loop = true;
                    } else {
                        loop = false;
                    }
                    if (display_border == 1) {
                        $zestard_recent_product(".recently_viewed").css("cssText", "border:" + size + " " + style + " " + color + " !important;padding:20px;margin:40px;");
                    } else {
                        $zestard_recent_product(".recently_viewed").css("cssText", "padding: 20px;margin:40px;");
                    }
                    $zestard_recent_product("#recent_view_slider .preview_title").html(slider_title);
                    $zestard_recent_product("#recent_view_slider .preview_subtitle").html(slider_subtitle);                    
					if (product_click == 1) {
                        for (var i = 0; i < length; i++) {
                            variants_select = "";
                            product_id = recently_viewed_products[i].product_id;
                            name = recently_viewed_products[i].product_name;							
                            if (name.length > 10)
							{
								//name = name.substring(0, 10) + "....";
							}
                            variants = JSON.parse(recently_viewed_products[i].variants);
                            variants_select = "<div class ='variants'><select data-id='" + product_id + "' class='product_variants' id='recent_view_" + product_id + "' ><option value=''>Select Variant</option>";
                            $zestard_recent_product.each(variants, function (key, value) {
								values = value.split(",");								
                                variants_select = variants_select + "<option data-id='" + values[1] + "' value='" + key + "'>" + values[0] + "</option>";
                            });
                            variants_select = variants_select + "</select></div>";
                            url = shop_name + '/products/' + recently_viewed_products[i].product_handle;
                            if(shop_name == "true-african-art-com.myshopify.com")
							{
								$zestard_recent_product(".recently_viewed").append("<div><div class ='recently_viewed_image'><a href='https://" + url + "'><img style='width:auto !important;height:auto !important;' src='" + recently_viewed_products[i].product_image + "'/></a></div><div class ='product_name'><a href='https://" + url + "'>" + name + "</a></div><br/>" 	+ variants_select 	+ "<div id='currency" + product_id + "' class ='currency'><b>" + shop_currency + " " + recently_viewed_products[i].product_price + "</b></div><div class ='add_to_cart_btn'><a data-id='" + product_id + "' class='product_add btn btn-info' >Add to Cart</a></div></div>");
							}
							else
							{
								$zestard_recent_product(".recently_viewed").append("<div><div class ='recently_viewed_image'><a href='https://" + url + "'><img style='width:200px !important;height:200px !important;' src='" + recently_viewed_products[i].product_image + "'/></a></div><div class ='product_name'><a href='https://" + url + "'>" + name + "</a></div><br/>" + variants_select 	+ "<div id='currency" + product_id + "' class='currency'><b>" + shop_currency + " " + recently_viewed_products[i].product_price + "</b></div><div class ='add_to_cart_btn'><a data-id='" + product_id + "' class='product_add btn btn-info' >Add to Cart</a></div></div>");
							}
                        }
                        recent_product_add();
                    } 
					else 
					{
                        for (var i = 0; i < length; i++) {
                            variants_select = "";
                            product_id = recently_viewed_products[i].product_id;
                            name = recently_viewed_products[i].product_name;							
                            if (name.length > 10)
							{
								//name = name.substring(0, 10) + "....";
							}
                            variants = JSON.parse(recently_viewed_products[i].variants);
                            variants_select = "<div class ='variants'><select data-id='" + product_id + "' class='product_variants' id='recent_view_" + product_id + "' ><option value=''>Select Variant</option>";
                            $zestard_recent_product.each(variants, function (key, value) {
								values = value.split(",");								
                                variants_select = variants_select + "<option data-id='" + values[1] + "' value='" + key + "'>" + values[0] + "</option>";
                            });
                            variants_select = variants_select + "</select></div>";
                            if(shop_name == "true-african-art-com.myshopify.com")
							{
								$zestard_recent_product(".recently_viewed").append("<div><div class ='recently_viewed_image'><img style='width:auto !important;height:auto !important;' class='product_click' data-popup-open='popup-3' data-id='" + product_id + "' src='" + recently_viewed_products[i].product_image + "'/></a><div class ='product_name'><a class='product_click' data-popup-open='popup-3' data-id='" + product_id + "'>" + name + "</a></div><br/>" 	+ variants_select 	+ "<div id='currency" + product_id + "' class='currency'>" + shop_currency + " " + recently_viewed_products[i].product_price + "</div><div class ='add_to_cart_btn'><label>&nbsp;</label><a data-id='" + product_id + "' class='product_add btn btn-info'>Add to Cart</a></div></div>");
							}
							else
							{
								$zestard_recent_product(".recently_viewed").append("<div><div class ='recently_viewed_image'><img style='width:200px !important;height:200px !important;' class='product_click' data-popup-open='popup-3' data-id='" + product_id + "' src='" + recently_viewed_products[i].product_image + "'/></a><div class ='product_name'><a class='product_click' data-popup-open='popup-3' data-id='" + product_id + "'>" + name + "</a></div><br/>" + variants_select 	+ "<div id='currency" + product_id + "' class='currency'>" + shop_currency + " " + recently_viewed_products[i].product_price + "</div><div class ='add_to_cart_btn'><label>&nbsp;</label><a data-id='" + product_id + "' class='product_add btn btn-info'>Add to Cart</a></div></div>");
							}
                        }
                        show_popup();
                        recent_product_add();
                        modal_recent_product_add();
                    }					
                    //$zestard_recent_product(".recently_viewed_products img").css("height", "200px");
                    $zestard_recent_product(".recently_viewed_products img").css("cursor", "pointer");
                    var flag = 0;
                    /* $(".owl-controls").append("<div class='owl-nav'><button type='button' role='presentation' class='owl-prev disabled'><span aria-label='Previous'>‹</span></button><button type='button' role='presentation' class='owl-next'><span aria-label='Next'>›</span></button></div><div class='owl-dots'><button role='button' class='owl-dot active'><span></span></button><button role='button' class='owl-dot'><span></span></button><button role='button' class='owl-dot'><span></span></button></div>");	
                     $('.owl-carousel').owlCarousel({
                     loop: true,
                     margin: 10,
                     dots: true,
                     responsiveClass: true,
                     items	: products,
                     nav     : true,
                     autoplay: true,									
                     afterAction: function(){															
                     },
                     responsive: {
                     0: {
                     items: 1,
                     nav: true
                     },
                     600: {
                     items: 2,
                     nav: false
                     },
                     900: {
                     items: products,
                     nav: true,
                     loop: false,
                     //margin: 20
                     }
                     }
                     }); */

                    /*$zestard_recent_product('.recently_viewed').flexslider({
                     animation: "slide",
                     animationLoop: loop,
                     slideshow: autoplay,
                     itemWidth: get_width(products),
                     start: function(slider){
                     $('.recently_viewed').resize();
                     },
                     minItems: getGridSize(products),
                     maxItems: getGridSize(products),
                     });						                     
                     */
                    $('.recently_viewed').slick({
                        dots: true,
                        infinite: false,
                        speed: 300,
                        slidesToShow: parseInt(products),
                        slidesToScroll: parseInt(products),
                        infinite: loop,
                        autoplay: autoplay,
                        responsive: [
							{
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    dots: true
                                }
                            },
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            },
                            {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
					if(shop_name == "tattoo2018.myshopify.com")
					{
						$(".recently_viewed_image img").css("max-width", "195px");
						$(".recently_viewed_image img").css("max-height", "125.1px");
						setTimeout(function(){
							console.log($(".slick-prev:before").css("color", "#f152ff !important"));
							console.log($(".slick-prev:before").css("background","white !important"));	
							console.log($(".slick-next:before").css("color", "#f152ff !important"));
							console.log($(".slick-next:before").css("background","white !important"));	
						}, 5000);
					}
                    $zestard_recent_product(".recently_viewed ul.slick-dots").css("display", "none");
                    $zestard_recent_product(".flex-next").html("<img height=20 width=20 src='" + base_path_recent_view + "image/right.png' />");
                    $zestard_recent_product(".flex-prev").html("<img height=20 width=20 src='" + base_path_recent_view + "image/left.png' />");
                    /* $zestard_recent_product(".slick-next").html("<img height=20 width=20 src='" + base_path_recent_view + "image/right.png' />");
                     $zestard_recent_product(".slick-prev").html("<img height=20 width=20 src='" + base_path_recent_view + "image/left.png' />");									 						 */
                    $zestard_recent_product(".flex-next").css("right", "10px");
                    $zestard_recent_product(".flex-next").css("position", "absolute");
                    $zestard_recent_product(".flex-prev").css("left", "0");
                    $zestard_recent_product(".flex-prev").css("position", "absolute");
					if(shop_name == "true-african-art-com.myshopify.com")
					{						
						$(".slick-prev:before").css("color", "#E4B704 !important");
						$(".slick-next:before").css("color", "#E4B704 !important");
						$(".slick-prev:before").css("background", "#7d0a1d !important");												
						$(".slick-next:before").css("background", "#7d0a1d !important");						
						$(".recently_viewed_image img").css("height", "auto !important");						
						$(".recently_viewed_image img").css("width", "auto !important");	
						$(".product_variants").remove();	
						$(".product_name").css("color","#7D0A1D !important");	
						$("a.product_add.btn.btn-info").css("color","#7D0A1D !important");	
						$("a.product_add.btn.btn-info").css("background","#e4b704 !important");	
					}                  
					/* 
						$zestard_recent_product(".recently_viewed .flex-direction-nav a:before").css("cssText", "color:" + color + " !important;");	
						$zestard_recent_product(".recently_viewed .flex-direction-nav a.flex-next:before").css("cssText", "color:" + color + " !important;");			  
                    */
                }
            } 
			else 
			{
                $zestard_recent_product("#recent_div_box").remove();
            }
        } else {
            $zestard_recent_product("#recent_div_box").remove();
        }
    } else {
        $zestard_recent_product("#recent_div_box").remove();
    }
}

function recent_product_add(){	
	$(".product_variants").change(function(){
		var id = $zestard_recent_product(this).attr("data-id");
		$(".recently_viewed #currency"+id).html(shop_currency + " " + $('option:selected', this).data("id"));
	});
    $zestard_recent_product(".product_add").on("click", function () {
        var id = $zestard_recent_product(this).attr("data-id");
        product_id = $zestard_recent_product("#recent_view_" + id).val();
        if (product_id == '') {
            alert('Please Select Variant');
        } else {
            $zestard_recent_product.ajax({
                url: '/cart/add.js',
                data: {quantity: 1, id: product_id},
                async: false
            });
            window.location.href = '/cart';
        }
    });
}

function modal_recent_product_add() {
    $zestard_recent_product(".product_add_modal").on("click", function () {
        var id = $zestard_recent_product(this).attr("data-id");
        product_id = $zestard_recent_product("#select_variant_div #" + id).val();
        if (product_id == '') {
            alert('Please Select Variant');
        } else {
            $zestard_recent_product.ajax({
                url: '/cart/add.js',
                data: {quantity: 1, id: product_id},
                async: false
            });
            window.location.href = '/cart';
        }
    });
}

function show_popup() {
    var targeted_popup_class;
    //Opening Modal
    $zestard_recent_product('#recent_view_slider .product_click').on('click', function (e) {
        /* $zestard_recent_product("#recent_view_popup_modal").css("display","block");
         $zestard_recent_product('html, body').animate({
         scrollTop: $zestard_recent_product("#recent_view_content").offset().top - 100 //+ $zestard_recent_product("#quick_buy_content").height() / 2
         }, 2000);
         
         targeted_popup_class = $zestard_recent_product(this).attr('data-popup-open');					
         $zestard_recent_product('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);							
         e.preventDefault(); */
    });
    //Closing Modal
    $zestard_recent_product('[data-popup-close]').on('click', function (e) {
        targeted_popup_class = $zestard_recent_product(this).attr('data-popup-close');
        $zestard_recent_product('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
        e.preventDefault();
    });

    var id = '',
            index;
    $zestard_recent_product("#recent_view_slider .product_click").on("click", function () {
        $zestard_recent_product("#recent_view_popup_modal").css("display", "block");
        $zestard_recent_product('html, body').animate({
            scrollTop: $zestard_recent_product("#recent_view_content").offset().top - 100 //+ $zestard_recent_product("#quick_buy_content").height() / 2
        }, 2000);

        targeted_popup_class = $zestard_recent_product(this).attr('data-popup-open');
        $zestard_recent_product('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
        //e.preventDefault();
        id = $zestard_recent_product(this).attr("data-id");
        length = recently_viewed_products.length;
        
        for (var i = 0; i < length; i++) {
            if (id == recently_viewed_products[i].product_id) {
                index = i;
            }
        }
        
        price = shop_currency + " " + recently_viewed_products[index].product_price;
        img_src = recently_viewed_products[index].product_image;
        name = recently_viewed_products[index].product_name;
        desc = recently_viewed_products[index].product_description;
        variant_id = recently_viewed_products[index].product_variant_id;
        variants = JSON.parse(recently_viewed_products[index].variants);
        variants_select = "<select id='" + id + "'><option value=''>Select Variant</option>";
        $zestard_recent_product.each(variants, function (key, value) {
            variants_select = variants_select + "<option value='" + key + "'>" + value + "</option>";
        });
        variants_select = variants_select + "</select>";
        add_to_cart = "<a data-id='" + id + "' class='product_add_modal btn btn-info' style='background:#7796a8;'>Add to Cart</a>";
        $zestard_recent_product("#recent_view_image").attr("src", img_src);
        $zestard_recent_product("#recent_view_content #product_name").html(name);
        $zestard_recent_product("#recent_view_content #product_price").html(price);
        $zestard_recent_product("#recent_view_content #product_desc").html(desc);
        $zestard_recent_product("#recent_view_content #add_to_cart_button").html(add_to_cart);
        $zestard_recent_product("#recent_view_content #select_variant_div").html(variants_select);
        modal_recent_product_add();
    });
}