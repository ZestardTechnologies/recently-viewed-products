<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('dashboard', 'callbackController@dashboard')->name('dashboard');

Route::get('callback', 'callbackController@index')->name('callback');

Route::any('update-modal-status', 'callbackController@update_modal_status')->name('update-modal-status');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');

Route::any('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success/{shop}', 'callbackController@payment_compelete')->name('payment_success');

Route::get('preview', 'previewController@index')->middleware('cors')->name('preview');

Route::any('save-recently-viewed', 'RecentlyViewedController@save_recently_viewed')->name('save-recently-viewed');

Route::any('quick-buy-products', 'RecentlyViewedController@quick_buy_products')->name('quick-buy-products');

Route::any('get-settings', 'RecentlyViewedController@get_settings')->middleware('cors')->name('get-settings');

Route::any('get-details', 'RecentlyViewedController@get_details')->middleware('cors')->name('get-details');

Route::any('get-products', 'RecentlyViewedController@get_products')->middleware('cors')->name('get-products');

Route::any('testing', 'RecentlyViewedController@testing')->middleware('cors')->name('testing');

Route::any('products', 'RecentlyViewedController@products')->name('products');

Route::any('save-session', 'RecentlyViewedController@save_session')->name('save-session');

Route::any('save-products', 'RecentlyViewedController@save_products')->name('save-products');

Route::any('save-auto-products', 'RecentlyViewedController@save_auto_products')->name('save-auto-products');

Route::any('acknowledge', 'RecentlyViewedController@acknowledge')->name('acknowledge');

Route::any('view-product', 'RecentlyViewedController@save_product_count')->name('view-product');

Route::any('update-ids', 'RecentlyViewedController@update_product_ids')->middleware('cors')->name('update-ids');

Route::any('testing', 'RecentlyViewedController@testing')->middleware('cors')->name('testing');

Route::get('help', function () {
    return view('help');
})->name('help');

Route::any('charge-declined', 'callbackController@charge_declined')->name('charge-declined');

/* For Best Seller */
Route::any('get-seller-data', 'RecentlyViewedController@get_bestseller_products')->middleware('cors')->name('get-seller-data');
Route::any('get-product-data', 'RecentlyViewedController@get_product_data')->middleware('cors')->name('get-product-data');
Route::any('best-seller', 'RecentlyViewedController@get_best_seller_view')->middleware('cors')->name('best-seller');

Route::get('track-products', function () {
    return view('track_products');
})->name('track-products');

/* For Most Viewed Products */
Route::any('get-most-product', 'RecentlyViewedController@get_mostviewed_products')->middleware('cors')->name('get-most-product');

/* For Recently Viewed Products */
Route::any('recent-product', 'RecentlyViewedController@save_recently_viewed_product')->middleware('cors')->name('recent-product');
