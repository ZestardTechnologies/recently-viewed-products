<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ShopModel;
use App\ProductsModel;
use App;
use DB;
use File;
use Mail;

class RecentlyViewedController extends Controller {

    public function get_details(Request $request) {
        echo $request->input('shop');
    }

    public function save_recently_viewed(Request $request) {
        $shop = session('shop');
        if (!empty($_GET['shop'])) {
            $shop = $_GET['shop'];
        }
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>
                    $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' =>
                    $select_store[0]->access_token]);
        $currency = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
        $shop_currency = $currency->shop->currency;
        $count = DB::table('recently_viewed_products_settings')->where('store', $shop)->count();

        if ($request->input('status')) {
            $status = 1;
        } else {
            $status = 0;
        }
        if ($request->input('select_products')) {
            $select_products = 1;
        } else {
            $select_products = 0;
        }
        if ($request->input('autoplay')) {
            $autoplay = 1;
        } else {
            $autoplay = 0;
        }
        if ($request->input('loop')) {
            $loop = 1;
        } else {
            $loop = 0;
        }
        if ($request->input('border_status')) {
            $border_status = 1;
        } else {
            $border_status = 0;
        }

        if ($count > 0) {
            $settings = array(
                'app_status' => $status,
                'slider_title' => $request->input('slider_title'),
                'slider_subtitle' => $request->input('slider_subtitle'),
                'number_of_products' => $request->input('product_no'),
                'select_products' => $select_products,
                'autoplay_slider' => $autoplay,
                'display_border' => $border_status,
                'border_style' => $request->input('border_style'),
                'border_color' => $request->input('border_color'),
                'border_size' => $request->input('border_size'),
                'loop' => $loop,
                'product_click' => $request->input('product_click'),
                'persist_day_week_month' => $request->input('persist_day_week_month'),
                'day_week_month' => $request->input('day_week_month')
            );
            DB::table('recently_viewed_products_settings')->where('store', $shop)->update($settings);
        } else {
            $settings = array(
                'store' => $shop,
                'app_status' => $status,
                'slider_title' => $request->input('slider_title'),
                'slider_subtitle' => $request->input('slider_subtitle'),
                'number_of_products' => $request->input('product_no'),
                'select_products' => $select_products,
                'autoplay_slider' => $autoplay,
                'display_border' => $border_status,
                'border_style' => $request->input('border_style'),
                'border_color' => $request->input('border_color'),
                'border_size' => $request->input('border_size'),
                'loop' => $loop,
                'product_click' => $request->input('product_click'),
                'persist_day_week_month' => $request->input('persist_day_week_month'),
                'day_week_month' => $request->input('day_week_month'),
                'shop_currency' => $shop_currency
            );
            DB::table('recently_viewed_products_settings')->insert($settings);
        }
        $notification = array(
            'message' => 'Settings Saved Successfully.',
            'alert-type' => 'success');
        return redirect()->route('dashboard')->with('notification', $notification);
    }

    public function acknowledge(Request $request) {
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $shop = session('shop');
        $error = $request->input('response');
        $message = "Following Analytics error generated in store " . $shop . ":\n" . $error . "\n";
        
    }

    public function get_best_seller_view(Request $request) {
        $shop_name = $request->shop_name;
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        if ($shop_find->status == "active") {
            return view('best_seller_view');
        } else {
            return "Page Not Found";
        }
    }

    public function get_settings(Request $request) {
        $shop = $request->input('shop_name');
        $settings = DB::table('recently_viewed_products_settings')->where('store', $shop)->get();
        return json_encode($settings);
    }

    public function save_auto_products(Request $request) 
    {
        session(['update_product_ids' => ""]);
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = $request->input('shop_name');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>
                    $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' =>
                    $select_store[0]->access_token]);
        $products = array_count_values($request->input('products'));
        arsort($products);
        $product_ids = array();
        foreach ($products as $key => $value) {
            array_push($product_ids, $key);
        }
			
        if (count($product_ids) < 6) {
            $ids = implode(",", $product_ids);
        } 
		else 
		{
			if($shop == "true-african-art-com.myshopify.com")
			{							
				$ids = implode(",", $product_ids);
			}	
			else
			{				
				$ids = implode(",", array_slice($product_ids, 0, 6));
			}
        }
        $variants = array();
        DB::table('quick_buy_products')->where("store", $shop)->delete();
        $shopify_products = $sh->call(['URL' => '/admin/products.json?ids=' . $ids . "&published_status=published", 'METHOD' => 'GET']);
        $i = 0;

        foreach ($shopify_products as $product) {
            foreach ($product as $attributes) {
                $variant_id = $attributes->variants[0]->id;
                foreach ($attributes->variants as $variant) {
                    $variants[$variant->id] = $variant->title;
                }
                if (empty($attributes->images)) {
                    $img_src = 'https://zestardshop.com/shopifyapp/quick_buy/public/image/no_image.png';
                } else {
                    $img_src = $attributes->images[0]->src;
                }
                $data = array(
                    'store' => $shop,
                    'product_id' => "$attributes->id",
                    'variants' => json_encode($variants),
                    'product_variant_id' => "$variant_id",
                    'product_name' => $attributes->title,
                    'product_price' => $attributes->variants[0]->price,
                    'product_image' => $img_src,
                    'product_handle' => $attributes->handle,
                    'product_description' => $attributes->body_html
                );
                DB::table('quick_buy_products')->insert($data);
                $i++;
            }
        }
    }

    public function get_product_data(Request $request) {
        $product_data = array();
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = $request->input('shop_name');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>
                    $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' =>
                    $select_store[0]->access_token]);
        $products = array_count_values($request->input('products'));
        arsort($products);
        $product_ids = array();
        foreach ($products as $key => $value) {
            array_push($product_ids, $key);
        }
        if(count($product_ids) < 6) 
		{
            $ids = implode(",", $product_ids);
        } 
		else 
		{
            if($shop == "true-african-art-com.myshopify.com")
			{							
				$ids = implode(",", $product_ids);
			}	
			else
			{				
				$ids = implode(",", array_slice($product_ids, 0, 6));
			}
        }
        $variants = array();
        DB::table('best_seller_products')->where("store", $shop)->delete();
        $shopify_products = $sh->call(['URL' => '/admin/products.json?ids=' . $ids . "&published_status=published", 'METHOD' => 'GET']);
        $i = 0;
        foreach ($shopify_products as $product) {
            foreach ($product as $attributes) {
                $data = array(
                    'product_name' => $attributes->title,
                    'quantity_sold' => $products[$attributes->id]
                );
                array_push($product_data, $data);
            }
        }
        echo json_encode($product_data);
    }

    /* For Most Viewed Prodcuts App */

    public function save_product_count(Request $request) {
        $product_id = $request->input('product_id');
        $product = DB::table("most_viewed_products")->where('product_id', $product_id)->first();
        if (count($product) > 0) {
            $count = $product->count + 1;
            DB::table("most_viewed_products")->where('product_id', $product_id)->update(['count' => $count]);
        } else {
            $data = array(
                'product_id' => $product_id,
                'count' => 1
            );
            DB::table("most_viewed_products")->insert($data);
        }
    }

    /* For Recently Viewed Products */

    public function save_recently_viewed_product(Request $request) {
        $product_ids = $request->input('product_ids');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = $request->input('shop_name');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $products = $sh->call(['URL' => '/admin/products.json?ids=' . $product_ids . "&published_status=published", 'METHOD' => 'GET']);
        $recent_products = array();
        foreach ($products as $product) {
            foreach ($product as $attributes) {
                $variants = array();
                $variant_id = $attributes->variants[0]->id;
                
                foreach ($attributes->variants as $variant) {
                    /* $variants[$variant->id] = $variant->title; */
                    $variants[$variant->id] = $variant->title . "," .  $variant->price;
                }
                if (empty($attributes->images)) {
                    $img_src = 'https://zestardshop.com/shopifyapp/quick_buy/public/image/no_image.png';
                } else {
                    $img_src = $attributes->images[0]->src;
                }
                
                /*
                 * changed $image_src to $image_src_medium to show medium thumbnail instead of full size image on 09-07-2018 by Mitva Shah
                 */
                $img_src_medium = $this->merge($attributes->images[0]->src, "_medium");
              
                $data = array(
                    'store' => $shop,
                    'product_id' => $attributes->id,
                    'variants' => json_encode($variants),
                    'product_variant_id' => "$variant_id",
                    'product_name' => $attributes->title,
                    'product_price' => $attributes->variants[0]->price,
                    'product_image' =>  $img_src_medium,
                    'product_handle' => $attributes->handle,
                    'product_description' => $attributes->body_html
                );
                array_push($recent_products, $data);
            }
        }
        $product_data = array();
        $expiry = $request->input('expiry');
        $product_data['expire_timestamp'] = $expiry;
        $product_data['product_data'] = $recent_products;
        return json_encode($product_data);
        //$product = DB::table("recently_viewed_products")->where('shop_name',$shop_name)->get();
    }
    
     public function merge($file, $language){
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $filename = str_replace('.'.$ext, '', $file).$language.'.'.$ext;
        return ($filename);
    }

    public function get_products(Request $request) {
        $shop = $request->input('shop_name');
        $products = ProductsModel::where('store', $shop)->get()->toArray();
        return json_encode($products);
    }

    /* For Best Seller */

    public function save_session(Request $request) {
        echo session('ids');
        echo $request->input('id');
        if ($request->input('flag') == 1) {
            if (empty(session('ids'))) {
                session(['ids' => $request->input('id')]);
            } else {
                $ids = session('ids') . "," . $request->input('id');
                session(['ids' => $ids]);
            }
        } else {
            if (empty(session('ids'))) {
                
            } else {
                $ids = session('ids');
                $array_ids = explode(",", $ids);
                $key = array_search($request->input('id'), $array_ids);
                $key;
                unset($array_ids[$key]);
                session(['ids' => implode(",", $array_ids)]);
            }
        }
    }

    public function update_product_ids(Request $request) {
        $updated_product_ids = $request->input('product_ids');
        session(['updated_product_ids' => implode(",", $updated_product_ids)]);
        echo session('updated_product_ids');
    }

    public function testing() {

        $shop = "zestardgiftshop.myshopify.com";
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->first();
        $shop_id = $select_store->id;
        $encrypt = crypt($shop_id, "ze");
        $finaly_encrypt = str_replace(['/', '.'], "Z", $encrypt);
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
        $hooks = $sh->call(['URL' => '/admin/webhooks.json', 'METHOD' => 'GET']);
        dd($hooks);
        foreach ($theme->themes as $themeData) {
            if ($themeData->role == 'main') {

                $snippets_arguments = ['id' => $finaly_encrypt];
                $theme_id = $themeData->id;
                $view = (string) View('snippets', $snippets_arguments);

                //api call for creating snippets
                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/recently-viewed.liquid', 'value' => $view]]]);
            }
        }
        dd($call);
        /* dd($sh->call(['URL' => '/admin/smart_collections.json','METHOD' => 'GET']));		 */
    }

}
