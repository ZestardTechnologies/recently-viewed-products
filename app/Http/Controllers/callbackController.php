<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\ProductsModel;
use Mail;

class callbackController extends Controller {

    public function index(Request $request) {
        $sh = App::make('ShopifyAPI');

        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if (!empty($_GET['shop'])) {
            $shop = $_GET['shop'];
            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

            if (count($select_store) > 0) {
                //Remove comment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges/' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('dashboard', ['shop' => $shop]);
                } else {
                    return redirect()->route('payment_process');
                }
            } else {
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
				
				$permission_url = $sh->installURL(['permissions' => array('read_themes', 'write_themes', 'read_products', 'read_analytics'), 'redirect' => $app_settings->redirect_url]);
				
                return redirect($permission_url);
            }
        }
    }

    public function redirect(Request $request) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if (!empty($request->input('shop')) && !empty($request->input('code'))) {
            $shop = $request->input('shop'); //shop name

            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
            if (count($select_store) > 0) {
                //Remove comment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('dashboard', ['shop' => $shop]);
                } else {
                    return redirect()->route('payment_process');
                }
            }
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
            try {
                $verify = $sh->verifyRequest($request->all());
                if ($verify) {
                    $code = $request->input('code');
                    $accessToken = $sh->getAccessToken($code);

                    DB::table('usersettings')->insert(['access_token' => $accessToken, 'store_name' => $shop, 'store_encrypt' => ""]);
                    $shop_find = ShopModel::where('store_name', $shop)->first();
                    $shop_id = $shop_find->id;

                    $donation_encrypt = crypt($shop_id, "ze");
                    $finaly_encrypt = str_replace(['/', '.'], "Z", $donation_encrypt);

                    $update_encrypt = ShopModel::where('id', $shop_id)->update(['store_encrypt' => $finaly_encrypt]);

                    $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

                    //for creating app uninstall webhook
                    $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
                    $webhookData = [
                        'webhook' => [
                            'topic' => 'app/uninstalled',
                            'address' => config('app.url') . 'uninstall.php',
                            'format' => 'json'
                        ]
                    ];
                    $uninstall = $sh->appUninstallHook($accessToken, $url, $webhookData);
                    //api call for get theme info
                    $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
                    foreach ($theme->themes as $themeData) {
                        if ($themeData->role == 'main') {

                            $snippets_arguments = ['id' => $finaly_encrypt];
                            $theme_id = $themeData->id;
                            $view = (string) View('snippets', $snippets_arguments);

                            //api call for creating snippets
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/recently-viewed.liquid', 'value' => $view]]]);
                        }
                    }
                    
                    session(['shop' => $shop]);

                    //creating the Recuring charge for app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    //if(stristr($shop, "zestard") && 1==2)                    				
                    if ($shop == "all-free-theme-test.myshopify.com" || $shop == "zankar-test.myshopify.com") { //recently-viewed-products-zestard.myshopify.com
                        $charge = $sh->call([
                            'URL' => $url,
                            'METHOD' => 'POST',
                            'DATA' => array(
                                'recurring_application_charge' => array(
                                    'name' => 'Recently Viewed Products on Cart Page',
                                    'price' => 0.01,
                                    'return_url' => url('payment_success/' . $shop),
                                    'test' => true
                                )
                            )
                                ], false);
                    } else {
                        $charge = $sh->call([
                            'URL' => $url,
                            'METHOD' => 'POST',
                            'DATA' => array(
                                'recurring_application_charge' => array(
                                    'name' => 'Recently Viewed Products on Cart Page',
                                    'price' => 4.99,
                                    'return_url' => url('payment_success/' . $shop),
                                    "capped_amount" => 30,
                                    "terms" => "Additional charges as per the plan",
                                //'test' => true
                                )
                            )
                                ], false);
                    }

                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);
                    $record_count = DB::table('recently_viewed_products_settings')->where('store', $shop)->count();
                    $currency = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
                    $shop_currency = $currency->shop->currency;
                    if ($record_count <= 0) {
                        DB::table('recently_viewed_products_settings')->insert(['store' => $shop, 'app_status' => 1, 'slider_title' => 'Recently Viewed Products', 'slider_subtitle' => '', 'product_click' => 1, 'number_of_products' => 4, 'autoplay_slider' => 1, 'loop' => 1, 'display_border' => 1, 'border_style' => 'solid', 'border_size' => '1px', 'border_color' => '#000000', 'persist_day_week_month' => 1, 'day_week_month' => '1', 'shop_currency' => $shop_currency]);
                    }

                    $shop_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);

                    //for the installation follow up mail for cliant
                    $subject ="Zestard Installation Greetings :: Recently Viewed Products";
                    $sender ="support@zestard.com";
                    $sender_name ="Zestard Technologies";
                    $app_name = "Recently Viewed Products";
                    $logo = config('app.url').'public/image/zestard-logo.png';
                    $installation_follow_up_msg ='<html>

                    <head>
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <style>
                            @import url("https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i");
                            @media only screen and (max-width:599px) {
                                table {
                                    width: 100% !important;
                                }
                            }
                            
                            @media only screen and (max-width:412px) {
                                h2 {
                                    font-size: 20px;
                                }
                                p {
                                    font-size: 13px;
                                }
                                .easy-donation-icon img {
                                    width: 120px;
                                }
                            }
                        </style>
                    
                    </head>
                    
                    <body style="background: #f4f4f4; padding-top: 57px; padding-bottom: 57px;">
                        <table class="main" border="0" cellspacing="0" cellpadding="0" width="600px" align="center" style="border: 1px solid #e6e6e6; background:#fff; ">
                            <tbody>
                                <tr>
                                    <td style="padding: 30px 30px 10px 30px;" class="review-content">
                                        <p class="text-align:left;"><img src="'.$logo.'" alt=""></p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px; line-height: 25px; margin-top: 0px;"><b>Hi '.$shop_info->shop->shop_owner.'</b>,</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Thanks for Installing Zestard Application '.$app_name.'</p>
                                        <p style="font-family: \'Helvetica\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">We appreciate your kin interest for choosing our application and hope that you have a wonderful experience.</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Please don\'t feel hesitate to reach us in case of any queries or questions at <a href="mailto:support@zestard.com" style="text-decoration: none;color: #1f98ea;font-weight: 600;">support@zestard.com</a>.</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">We also do have live chat support services for quick response and resolution of queries.</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">(Please Note: Support services are available according to the IST Time Zone(i.e GMT 5:30+) as we reside in India. Timings are from 10:00am to 7:00pm)</p>
                    
                                    </td>
                                </tr>
                    
                                <tr>
                                    <td style="padding: 20px 30px 30px 30px;">
                    
                                        <br>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 26px; margin-bottom:0px;">Thanks,<br>Zestard Support</p>
                                    </td>
                                </tr>
                    
                            </tbody>
                        </table>
                    </body>';
        
                    $receiver =$shop_info->shop->email;
                    
                        try {
                            Mail::raw([], function ($message) use($sender,$sender_name,$receiver,$subject,$installation_follow_up_msg) {
                                $message->from($sender,$sender_name);
                                $message->to($receiver)->subject($subject);
                                $message->setBody($installation_follow_up_msg, 'text/html');
                            });
                        }catch (\Exception $ex) {
                            echo "error";
                            
                        }
                    


                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    $msg = '<table>
                            <tr>
                                <th>Shop Name</th>
                                <td>' . $shop_info->shop->name . '</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>' . $shop_info->shop->email . '</td>
                            </tr>
                            <tr>
                                <th>Domain</th>
                                <td>' . $shop_info->shop->domain . '</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>' . $shop_info->shop->phone . '</td>
                            </tr>
                            <tr>
                                <th>Shop Owner</th>
                                <td>' . $shop_info->shop->shop_owner . '</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>' . $shop_info->shop->country_name . '</td>
                            </tr>
                            <tr>
                                <th>Plan</th>
                                <td>' . $shop_info->shop->plan_name . '</td>
                            </tr>
                          </table>';

                    //mail("support@zestard.com","Recently Viewed Products on Cart Page App Installed",$msg,$headers);                                        
                    

                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } else {
                    // Issue with data
                }
            } catch (Exception $e) {
                echo '<pre>Error: ' . $e->getMessage() . '</pre>';
            }
        }
    }

    public function dashboard() {
        $shop = session('shop');
        if (!empty($_GET['shop'])) {
            $shop = $_GET['shop'];
            session(['shop' => $shop]);
        }
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('store_name', $shop)->first();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

        $access_token = $shop_find->access_token;
        $new_install = $shop_find->new_install;
        $settings = DB::table('recently_viewed_products_settings')->where('store', $shop)->get();
        $currency = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
        $country_currency = $currency->shop->currency;

        return view('dashboard', array('shopdomain' => $shop_find, 'settings' => $settings, 'currency' => $country_currency, 'new_install' => $new_install));
    }

    public function update_modal_status(Request $request) {
        $shop = $request->input('shop_name');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $shop_find->new_install = 'N';
        $shop_find->save();
    }

    public function payment_method(Request $request) {
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        if (count($select_store) > 0) {
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

            $charge_id = $select_store[0]->charge_id;
            $url = 'admin/recurring_application_charges/' . $charge_id . '.json';
            $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
            if (count($charge) > 0) {
                if ($charge->recurring_application_charge->status == "pending") {
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "declined" || $charge->recurring_application_charge->status == "expired") {
                    //creating the new Recuring charge after declined app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array(
                            'recurring_application_charge' => array(
                                'name' => 'Recently Viewed Products on Cart Page',
                                'price' => 4.99,
                                'return_url' => url('payment_success/' . $shop),
                            //'test' => true
                            )
                        )
                            ], false);

                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);

                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "accepted") {

                    $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
                    $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
                    $Activatecharge_array = get_object_vars($Activate_charge);
                    $active_status = $Activatecharge_array['recurring_application_charge']->status;
                    $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
                    return redirect()->route('dashboard', ['shop' => $shop]);
                }
            }
        }
    }

    public function payment_compelete(Request $request, $shop) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        //$shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $charge_id = $_GET['charge_id'];
        $url = 'admin/recurring_application_charges/#{' . $charge_id . '}.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET',]);
        $status = $charge->recurring_application_charges[0]->status;

        $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $status]);

        if ($status == "accepted") {
            $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
            $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
            $Activatecharge_array = get_object_vars($Activate_charge);
            $active_status = $Activatecharge_array['recurring_application_charge']->status;
            $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
            return redirect()->route('dashboard', ['shop' => $shop]);
            return redirect()->route('dashboard', ['shop' => $shop]);
        } elseif ($status == "declined") {
            return redirect()->route('charge-declined');
            //echo '<script>window.top.location.href="https://'.$shop.'/admin/apps"</script>';
        }
    }

    public function charge_declined(Request $request) {
        return view('charge_declined')->with('store_name', session('shop'));
    }

}
